<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Event;

class ParticipantTest extends TestCase
{
    use WithFaker;

    public function testCreate()
    {
        $user = factory(User::class)->create();
        $event = factory(Event::class)->create();

        $data = [
            'name' => $this->faker->name,
            'surname' => $this->faker->lastName,
            'email' => $this->faker->email,
            'events' => [$event->id]
        ];

        $response = $this->actingAs($user, 'api')
                         ->postJson('api/participants', $data);

        $response->assertJson([
            'status' => 'success',
            'participant' => [
                'name' => $data['name'],
                'surname' => $data['surname'],
                'email' => $data['email'],
            ]
        ]);
    }

    public function testUpdate()
    {
        $user = factory(User::class)->create();
        $event = factory(Event::class)->create();

        $participant = factory(\App\Participant::class)->create();

        $data = [
            'name' => $this->faker->name,
            'surname' => $this->faker->lastName,
            'events' => [$event->id]
        ];

        $response = $this->actingAs($user, 'api')
            ->putJson('api/participants/' . $participant->id, $data);

        $response->assertStatus(200)
                 ->assertJson([
                    'participant' => [
                        'name' => $data['name'],
                        'surname' => $data['surname']
                    ],
                    'status' => 'success'
                 ]);
    }
}
