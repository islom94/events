<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = ['name', 'date', 'city'];

    public function participants()
    {
        return $this->belongsToMany('App\Participant');
    }
}
