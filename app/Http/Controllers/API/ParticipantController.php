<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Participant;
use App\Http\Resources\ParticipantCollection;
use App\Http\Resources\ParticipantResource;
use Illuminate\Support\Facades\Validator;
use App\Event;
use App\Jobs\Logging;

class ParticipantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $participants = Participant::all();
        $event = $request->query('event');
        if ($event) {
            $participants = Event::where('name', $event)->get();
        }

        return response()->json(['status' => 'success',
            'participants' => new ParticipantCollection($participants)]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'name' => 'required|max:255',
            'surname' => 'required|max:255',
            'email' => 'email|required|unique:participants',
            'events' => 'required|array|min:1'
        ]);

        if ($validator->fails()) {
            return response(['status' => 'error', 'message' => $validator->errors()->first()]);
        }

        $participant = Participant::create($data);

        Logging::dispatch($participant);

        if (isset($data['events'])) {
            $participant->events()->sync($data['events']);
        }


        return response()->json(['status' => 'success', 'participant' => new ParticipantResource($participant)]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $participant = Participant::find($id);

        return response()->json(['status' => 'success', 'participant' => new ParticipantResource($participant)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'name' => 'min:3|max:255',
            'surname' => 'min:3|max:255',
            'events' => 'array|min:1'
        ]);

        if ($validator->fails()) {
            return response(['status' => 'error', 'message' => $validator->errors()->first()]);
        }

        $participant = Participant::find($id);

        if (isset($data['events'])) {
            $participant->events()->sync($data['events']);
        }

        $participant->update($data);

        return response()->json(['status' => 'success', 'participant' => new ParticipantResource($participant)]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Participant::destroy($id);

        return response()->json(['status' => 'success']);
    }
}
