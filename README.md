# Инструкция по проекту

## Установка

После запуска миграций вводим команду:

```bash
php artisan passport:install
```

Для заполнения БД мероприятиями и участниками вводим:

```bash
php artisan db:seed
```

## Использование

Для использования АПИ сначала нужно авторизоваться и получить токен. Для этого есть следующие роуты:

POST /api/register (регистрация пользователя)- передаваемые параметры: name(имя), email(почта), password(пароль). В ответ получаем параметр token который исользуем в заголовках запросов как Bearer токен

POST /api/login (вход) - передаваемые параметры: email(почта), password(пароль). В ответ получаем параметр token который исользуем в заголовках запросов как Bearer токен

Роуты для АПИ участников:

GET api/participants - получить список участников

POST api/participants - создать участника, передаваемые параметры: name(имя), email(почта), password(пароль), events(ID мероприятий, массив)

PUT|PATCH api/participants/{participant_id} - обновить данные, participant_id - ID участника(пример: api/participants/55) передаваемые параметры: name(имя), password(пароль), events(ID мероприятий, массив)

DELETE api/participants/{participant_id} - удалить участника, participant_id - ID участника(пример: api/participants/55)
