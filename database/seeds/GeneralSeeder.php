<?php

use Illuminate\Database\Seeder;
use App\Event;
use App\Participant;

class GeneralSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Event::class, 50)->create();
        factory(Participant::class, 50)->create()->each(function($item) {
            $item->events()->attach(Event::all()->random(1));
        });
    }
}
